let container = document.querySelector("#container");
let dino = document.querySelector("#dino");
let block = document.querySelector("#block");
let road = document.querySelector("#road");
let cloud = document.querySelector("#cloud");
let score = document.querySelector("#score");
let gameOver = document.querySelector("#gameOver");

//devlaration de variable pour le score

let interval = null;
let playerScore = 0;

// fonction pour le score 
let scoreCounter = ()=>{
    playerScore ++;
    score.innerHTML = `Score <b>${playerScore}</b>`;
}

//start game
// interval = setInterval(scoreCounter, 200);
window.addEventListener("keydown", (start)=>{
    if(start.code == "Space") 
    {
        gameOver.style.display= "none";
        block.classList.add("blockActive");
        road.firstElementChild.style.animation = "roadAnimate 1.5s linear infinite";
        cloud.firstElementChild.style.animation = "cloudAnimate 50s linear infinite";


        //score
        let playerScore = 0;
        interval = setInterval(scoreCounter, 200);
    }
});


//fait jumper ton personnage 
window.addEventListener("keydown" , (e)=> {

    if(e.key == "ArrowUp")
        if(dino.classList != "dinoActive")
        {
            dino.classList.add("dinoActive");

            //remove la class après 0.5 seconde
            setTimeout(()=>{
                dino.classList.remove("dinoActive");
            }, 500);

        }
});

//'game over' si le Character  hit le block
let result = setInterval(()=>{
    let dinoBottom = parseInt(getComputedStyle(dino).getPropertyValue("bottom"));
    // console.log("dinoBottom" + dinoBottom);

    let blockLeft = parseInt(getComputedStyle(block).getPropertyValue("left"));
    // console.log("BlockLeft" + blockLeft);
    if(dinoBottom <= 90 && blockLeft >= 20 && blockLeft <= 145){
        gameOver.style.display = "block";
        block.classList.remove("blockActive");
        road.firstElementChild.style.animation = "none";
        cloud.firstElementChild.style.animation = "none";
        clearInterval(interval);
        playerScore =0;

    }
},10);
const dino = document.body.querySelector('div[id="dino"]');

const cactus = document.body.querySelector('div[id="cactus"]');

const death_counter_cell = document.body.querySelector('td');

const FEET_TOUCH_CACTUS = parseInt(window.getComputedStyle(dino).getPropertyValue('height'), 10);

const TAIL_TOUCH_CACTUS = parseInt(window.getComputedStyle(dino).getPropertyValue('left'), 10);

// console.log(FEET_TOUCH_CACTUS);

// console.log(TAIL_TOUCH_CACTUS);

const HEAD_TOUCH_CACTUS = TAIL_TOUCH_CACTUS + 60;

let dino_death_counter = 0;

const jump_events = ['mousedown', 'keyup'];

jump_events.forEach((event) => document.addEventListener(event, dinoJump));

setInterval(() => isDinoDead(), 100);

/**
 * makes the dino's jumping
 * @param {string} key The key pressed if exists
 * 
 */


function dinoJump({key}) {
    if ((key && key !== 'ArrowUp')) {
        return;
    }

    dino.classList.add('dino-jump');

    //////////////////////////////////////////////////////////

    dino.addEventListener('animationend', () => dino.classList.remove('dino-jump'));

}

function isDinoDead() {
    const dino_position_bottom = parseInt(window.getComputedStyle(dino).getPropertyValue('bottom'),10);

    const cactus_position_left = parseInt(window.getComputedStyle(cactus).getPropertyValue('left'), 10);

    if (
        (dino_position_bottom <= FEET_TOUCH_CACTUS)
        && (cactus_position_left <= HEAD_TOUCH_CACTUS)
        && (cactus_position_left >= TAIL_TOUCH_CACTUS)
    ) {
        dinoIsDead();
    } else {
        dinoIsAlive();
    }
}

function dinoIsDead() {
    dino_death_counter  += 1;

    death_counter_cell.textContent = dino_death_counter;

    dino.classList.add('dead');
}   


function dinoIsAlive() {
    dino.classList.remove('dead');
}